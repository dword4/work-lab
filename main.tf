provider "aws" {
        region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "itsn-tf-state"
    region = "us-east-1"
    key = "work-lab-terraform.state"
  }
}
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "main"
  }
}
/*
data "aws_vpc" "selected" {
  filter {
    name = "tag:Name"
    values = ["main"]
  }
}
*/

# add a ssh key

resource "aws_key_pair" "arakkis" {
    key_name = "arakkis"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLtI1AR6N/+nBU2BOFm3pOejzdA3CZWgigxqhxot2YOWHcPqthEusfNVQTRRGKot2esUOS15pzc3OulYLTgXUqTBWllQIv9CHy5PcuCB4aHIYLdSH57IS4JZMn87YIvPC75t2VZ259d6ZQ/UwO04EWt1iifA4OyJmbkO+6QOKM0n5TQaWkWXBX2zhRoVyTC+WnL3QSW1uTA+VoJa+/YH1sfRkNDnVNZ+5ZmG5x+e9C6qYr8CEJXVls9aCU0p/nMENYX6xWd1cQl3mNCHQ6MIxYYgiQHgj3P7qHeYg2ouC+H4ZQfsZgee8fwlLo7KF6G4Cq9ULd7aaIOkzUtd9MTYVeBXlPMruDNis5skQLNugBptlm6IKWXCXAZ5bjtbxWWAt1PB3VIbjU8G54Su+p0Vf6XjXawJeXdXdYu7GclStOKkKwfp0P2nGOl3NUeYdhoLpNXD39TIMVJxy7/ELiotEzIAZk6LrYnWg9ziEP28EaCKDIVFurLC71M8cscT76LJ8= dhynes@localhost.localdomain"
}
########################################
# lets build something in EC2 now
########################################


resource "aws_instance" "web" {
  ami           = "ami-0affd4508a5d2481b"
  instance_type = "t2.micro"
  key_name = "arakkis"
  tags = {
    Name = "HelloWorld"
  }
}
/*
output "vpcid" {
  value = "${data.aws_vpc.selected.id}"
}
*/
